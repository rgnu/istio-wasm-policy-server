/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (client) => ({request}) => {
    const [ none, namespace, name ] = request.url.split('/')

    console.info(`Fetching ${name} WASM policy from ${namespace} namespace`)
    
    return client.api.v1.namespace(namespace).configmap(name).get().then((result) => {
        return {
            statusCode: result.statusCode,
            headers: { 'Content-Type': 'application/octet-stream' },
            body: Buffer.from(result.body.binaryData.data, 'base64')
        }
    })
}
