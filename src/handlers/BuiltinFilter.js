const fs = require('fs')
const errors = require('../errors')

/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (mountPath) => ({request}) => {

    if (request.url !== mountPath ) throw new errors.UnhandledRequestError(request);

    console.info(`Fetching Builtin WASM policies`)

    return {
        statusCode: 200,
        headers: { 'Content-Type': 'application/octet-stream' },
        body: fs.readFileSync(`${__dirname}/../../build/optimized.wasm`)
    }
}
