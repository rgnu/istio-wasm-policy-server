const { isException } = require('../utils')
const { UnhandledRequestError } = require('../errors')

/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (client, handlers) => (request, response) => {
    return Promise.resolve({request, response})
    .then(handlers.BuiltinFilter('/_/builtin-wasm-filter'))
    .catch(isException(UnhandledRequestError, handlers.RandomStatus('/_/test/check')))
    .catch(isException(UnhandledRequestError, handlers.ConfigMapFilter(client)))
    .catch(isException(UnhandledRequestError, handlers.UnhandledRequest()))
    .catch(handlers.MapErrorToResponse())
    .then(handlers.Response(response))
}
