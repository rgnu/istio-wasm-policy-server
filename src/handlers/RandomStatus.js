const fs = require('fs')
const errors = require('../errors')

const statusCodes = [
    { code: 200, name: 'OK'},
    { code: 401, name: 'Unauthorized'},
    { code: 429, name: 'Too Many Requests'}
]

/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (mountPath) => ({request}) => {
    const statusCode = statusCodes[Math.floor(Math.random() * statusCodes.length)]

    if (request.url !== mountPath ) throw new errors.UnhandledRequestError(request);

    return {
        statusCode: statusCode.code,
        headers: { 'Content-Type': 'text/plain' },
        body: statusCode.name
    }
}
