const fs = require('fs')
const errors = require('../errors')

/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = () => ({ request }) => {
    const error = new errors.NotFoundHttpResponseError(request.url)
    throw error
}
