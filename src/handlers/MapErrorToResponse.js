/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = () => (error) => {
    console.error(error.toString())
    return {
        statusCode: error.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: error.toString()
    }
}
