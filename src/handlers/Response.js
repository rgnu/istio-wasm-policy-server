/**
 * 
 * @param type The action taked
 * @param object Object
 */
module.exports = (response) => (result) => {
    response.writeHead(result.statusCode,  result.headers)
    response.write(result.body)
    response.end()
}
