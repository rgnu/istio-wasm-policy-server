
function isException(expected, cb) {
    return function (actual) {
        if (typeof expected === 'function' && actual instanceof expected) {
            return cb(actual);
        }

        if (expected instanceof RegExp && expected.test(actual.message)) {
            return cb(actual);
        }

        if (actual.name == expected) {
            return cb(actual);
        }

        if (actual.message == expected) {
            return cb(actual);
        }

        throw actual;
    };
}

module.exports = {
    isException
}