
class DomainError extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        // This clips the constructor invocation from the stack trace.
        // It's not absolutely essential, but it does make the stack trace a little nicer.
        //  @see Node.js reference (bottom)
        Error.captureStackTrace(this, this.constructor);
    }
}

class UnhandledRequestError extends DomainError {
    constructor(request) {
        super("Unhandled Request")
        this.request = request
    }
}

class HttpResponseError extends DomainError {
    constructor(message, statusCode = 500) {
        super(message)
        this.statusCode = statusCode
    }
}

class NotFoundHttpResponseError extends HttpResponseError {
    constructor(resource) {
        super(`${resource} not found`, 404)
    }
}

module.exports = {
    DomainError,
    HttpResponseError,
    NotFoundHttpResponseError,
    UnhandledRequestError
}