// content of index.js
const http = require('http')
const Client = require('kubernetes-client').Client
const handlers = require('../src/handlers')

async function main() {
    const port = process.env.PORT || 3000
    const client = new Client({ version: '1.12' })
    const server = http.createServer(handlers.Request(client, handlers))

    server.listen(port, (err) => {
      if (err) {
        return console.error('something bad happened', err)
      }
    
      console.log(`server is listening on ${port}`)
    })
}

main().catch(console.error)