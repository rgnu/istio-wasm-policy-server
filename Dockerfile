FROM node:10-alpine
LABEL maintainer="rodrigo@rgnu.com.ar"

ENV HOME=/opt/app NODE_ENV=production PORT=3000 PWD=/opt/app

WORKDIR $HOME

COPY . $HOME/

RUN npm install --dev \
    && npm test \
    && npm run build \
    && npm prune --production \
    && npm cache clean --force

EXPOSE $PORT

CMD [ "node", "cmd/wasm-policy-server" ]
