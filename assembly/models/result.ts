
export class Result<T, E extends Error> {
    private _ok: T;
    private _err: E;

    ok(ok: T): Result<T, E> {
        this._ok = ok;
        return this;
    }

    err(err: E): Result<T, E> {
        this._err = err;
        return this;
    }


    isOk(): bool {
        return this._err == null;
    }

    isErr(): bool {
        return this._err != null;
    }

    unwrap(): T {
        if (this.isErr()) {
            throw this._err
        }
    }

    or(res: Result<T, E>): Result<T, E> {
        if (this.isOk()) {
            return this;
        }
        return res;
    }

    and(res: Result<any, Error>): Result<any, Error> {
        if (this.isOk()) {
            return res;
        }

        return this;
    }

    expect(e: Error): T {
        if (this.isErr()) {
            throw e;
        }

        return this.unwrap();
    }
}