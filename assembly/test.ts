import { Context, ContextHelper, FilterHeadersStatusValues, FilterMetadataStatusValues } from "@solo-io/proxy-runtime";
import { BaseContext, BaseRoot } from "./base";

export class TestRoot extends BaseRoot {

  createContext(context_id: u32): Context {
    this.logDebug("createContext() Called");

    return ContextHelper.wrap(new Test(context_id, this));
  }

  onConfigure(size: usize): bool {
    this.logDebug("onConfigure() Called");
    return true;
  }

  onTick(): void {
    this.logDebug("onTick() Called");
  }
}

class Test extends BaseContext<TestRoot> {

  onRequestMetadata(a: u32): FilterMetadataStatusValues {
    this.logDebug("onRequestMetadata() Called");

    return FilterMetadataStatusValues.Continue;
  }

  onResponseMetadata(a: u32): FilterMetadataStatusValues {
    this.logDebug("onResponseMetadata() Called");

    return FilterMetadataStatusValues.Continue;
  }
  
  onRequestHeaders(a: u32): FilterHeadersStatusValues {
    const headers: string[] = this.getRequestHeaders()
    .map<string>((value) => String.UTF8.decode(value.key) + "=" + String.UTF8.decode(value.value))

    this.logDebug("onRequestHeaders() Called RequestHeaders=" + headers.toString());

    return FilterHeadersStatusValues.Continue;
  }
  
  onResponseHeaders(a: u32): FilterHeadersStatusValues {
    const headers: string[] = this.getResponseHeaders()
    .map<string>((value) => String.UTF8.decode(value.key) + "=" + String.UTF8.decode(value.value))

    this.logDebug("onResponseHeaders() ResponseHeaders=" + headers.toString());

    return FilterHeadersStatusValues.Continue;
  }

  onLog(): void {
    const headers: string[] = this.getResponseHeaders()
    .map<string>((value) => String.UTF8.decode(value.key) + "=" + String.UTF8.decode(value.value))

    const msg = ["onLog()"]

    msg.push("ClusterName=" + this.getRootContext().getStringProperty(["cluster_name"]));
    msg.push("Path=" + this.getRootContext().getStringProperty(["request", "url_path"]));
    msg.push("ResponseHeaders=" + headers.toString());
    msg.push("ResponseSize=" + this.getRootContext().getInt64Property(["response", "total_size"]).toString());
 
    this.logDebug(msg.join(" "));
  }

  onDone(): bool {
    this.logDebug("onDone()");

    return true;
  }
}
