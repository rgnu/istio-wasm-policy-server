export * from "@solo-io/proxy-runtime/proxy";
import { RootContextHelper, registerRootContext } from "@solo-io/proxy-runtime";

import { DenierRoot } from "./denier"
import { TestRoot } from "./test";
import { AllowerRoot } from "./allower";
import { AddRequestIdHeaderRoot } from "./addRequestIdHeader";
import { ExtAuthRoot } from "./extauth";

registerRootContext((context_id: u32) => { return RootContextHelper.wrap(new TestRoot(context_id)); }, "test");
registerRootContext((context_id: u32) => { return RootContextHelper.wrap(new DenierRoot(context_id)); }, "denier");
registerRootContext((context_id: u32) => { return RootContextHelper.wrap(new AllowerRoot(context_id)); }, "allower");
registerRootContext((context_id: u32) => { return RootContextHelper.wrap(new AddRequestIdHeaderRoot(context_id)); }, "add_requestid_header");
registerRootContext((context_id: u32) => { return RootContextHelper.wrap(new ExtAuthRoot(context_id)); }, "extauth");
