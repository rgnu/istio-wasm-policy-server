import { RootContext, Context, LogLevelValues, WasmResultValues, send_local_response, HeaderPair, Headers, GrpcStatusValues, stream_context } from "@solo-io/proxy-runtime";
import { log, get_property } from "@solo-io/proxy-runtime/runtime"

export class BaseRoot extends RootContext {

  getContextId(): u32 {
    return this.context_id;
  }

  log(level: LogLevelValues, msg: string): void {
    log(level, "context id: " + this.getContextId().toString() + ": " + msg)
  }

  logDebug(msg: string): void {
    this.log(LogLevelValues.debug, msg);
  }

  logInfo(msg: string): void {
    this.log(LogLevelValues.info, msg);
  }

  logWarn(msg: string): void {
    this.log(LogLevelValues.warn, msg);
  }

  logError(msg: string): void {
    this.log(LogLevelValues.error, msg);
  }

  getProperty(path: string[]): ArrayBuffer {
    return get_property(path.join("\0"));
  }

  getDataViewProperty(path: string[]): DataView {
    return new DataView(this.getProperty(path), 0);
  }

  getInt64Property(path: string[]): i64 {
    return this.getDataViewProperty(path).getInt64(0, true);
  }

  getUInt64Property(path: string[]): u64 {
    return this.getDataViewProperty(path).getUint64(0, true);
  }

  getInt32Property(path: string[]): i32 {
    return this.getDataViewProperty(path).getInt32(0, true);
  }

  getUInt32Property(path: string[]): u32 {
    return this.getDataViewProperty(path).getUint32(0, true);
  }  

  getStringProperty(path: string[]): string {
    return String.UTF8.decode(this.getProperty(path));
  }

  getDateProperty(path: string[]): Date {
    return new Date(this.getInt64Property(path));
  }

  sendHttpResponse(status: u32, statusDetail: string, body: ArrayBuffer, headers: Headers): WasmResultValues {
    return send_local_response(status, statusDetail, body, headers, GrpcStatusValues.Unknown);
  }
}

export class BaseContext<T extends BaseRoot> extends Context {
  root_context : T;

  constructor(context_id: u32, root_context: T) {
    super(context_id, root_context);
    this.root_context = root_context;
  }

  getRootContext(): T {
    return this.root_context;
  }

  getContextId(): u32 {
    return this.context_id;
  }

  log(level: LogLevelValues, msg: string): void {
    this.root_context.log(level, msg)
  }

  logDebug(msg: string): void {
    this.root_context.logDebug(msg);
  }

  logInfo(msg: string): void {
    this.root_context.logInfo(msg);
  }

  logWarn(msg: string): void {
    this.root_context.logWarn(msg);
  }

  logError(msg: string): void {
    this.root_context.logError(msg);
  }

  getRequestHeaders(): Headers {
    return stream_context.headers.request.get_headers()
  }

  getRequestHeader(name: string): string {
    return stream_context.headers.request.get(name);
  }

  addRequestHeader(name: string, value: string): void {
    stream_context.headers.request.add(name, value);
  }

  getResponseHeaders(): Headers {
    return stream_context.headers.response.get_headers()
  }

  getResponseHeader(name: string): string {
    return stream_context.headers.response.get(name);
  }

  addResponseHeader(name: string, value: string): void {
    stream_context.headers.response.add(name, value);
  }


  getHttpCallbackHeaders(): Headers {
    return stream_context.headers.http_callback.get_headers();
  }

  getHttpCallbackHeader(name: string): string {
    return stream_context.headers.http_callback.get(name);
  }

  createHeader(key: string, value: string): HeaderPair {
    const header = new HeaderPair();
    header.key = String.UTF8.encode(key);
    header.value = String.UTF8.encode(value);

    return header
  }
}
