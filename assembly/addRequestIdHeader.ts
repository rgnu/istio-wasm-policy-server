import { Context, ContextHelper, FilterHeadersStatusValues } from "@solo-io/proxy-runtime";
import { BaseRoot, BaseContext } from "./base";

export class AddRequestIdHeaderRoot extends BaseRoot {
  headerName : string;

  onConfigure(size: usize): bool {
    super.onConfigure(size);
    this.headerName = this.getConfiguration();

    if (this.headerName == "") {
      this.headerName = "x-correlation-id";
    }

    this.logDebug("onConfigure() Called Config=" + this.headerName);

    return true;
  }

  createContext(context_id: u32): Context {
    return ContextHelper.wrap(new AddRequestIdHeader(context_id, this));
  }
}

class AddRequestIdHeader extends BaseContext<AddRequestIdHeaderRoot> {

  onRequestHeaders(a: u32): FilterHeadersStatusValues {
    const requestID    = this.getRequestHeader('x-request-id');

    this.logDebug("onRequestHeaders() Called with RequestID=" + requestID)

    this.addRequestHeader(this.getRootContext().headerName, requestID)

    return FilterHeadersStatusValues.Continue
  }
}
