import { Context, ContextHelper, FilterHeadersStatusValues, WasmResultValues, FilterDataStatusValues, FilterTrailersStatusValues } from "@solo-io/proxy-runtime";
import { BaseContext, BaseRoot } from "./base";

export class ExtAuthRoot extends BaseRoot {
  config: ExtAuthConfig;

  createContext(context_id: u32): Context {
    this.logDebug("createContext() Called");

    return ContextHelper.wrap(new ExtAuth(context_id, this));
  }

  onConfigure(size: usize): bool {
    super.onConfigure(size);
    const config = this.getConfiguration().split("|");

    this.config = new ExtAuthConfig(config[0], config[1], config[2]);

    this.logDebug("onConfigure() Called Config=" + this.config.toString());

    return true;
  }

  getConfig(): ExtAuthConfig {
    return this.config;
  }
}

class ExtAuthConfig {
  domain: string
  port: string
  path: string

  constructor(domain: string, port: string = "80", path: string = "/") {
    this.domain = domain;
    this.port = port;
    this.path = path;
  }

  getCluster(): string {
    return "outbound|" + this.port + "||" + this.domain
  }

  getService(): string {
    return this.path
  }

  toString(): string {
    return "ExtAuthConfig[" + 
      "domain=" + this.domain + 
      ",port=" + this.port + 
      ",path=" + this.path + 
    "]"  
  }
}

const checkCallCb = (origin_context: Context, headers: u32, body_size: usize, trailers: u32): void => {
  let context = origin_context as ExtAuth;
  
  context.setEffectiveContext();
  
  if (headers != 0) {
    const status = context.getHttpCallbackHeader(":status");
    // if we have a response, allow the request if we have a 200
    context.logDebug("header count: " + headers.toString() + ", body_size: " + body_size.toString() + ", status: " + status);
    if (status != "200") {
      context.logWarn("checkCallCb() Failed with Status=" + status + " ResponseHeaders=" + context.getHttpCallbackHeaders()
      .map<string>((value) => String.UTF8.decode(value.key) + "=" + String.UTF8.decode(value.value)).toString());
      context.getRootContext().sendHttpResponse(parseInt(status) as u32, "Check Failed", new ArrayBuffer(0), []);
      context.finishRequest();
      return;
    }
  }

  // set the context back to the original request
  context.continueRequest();
}

class ExtAuth extends BaseContext<ExtAuthRoot> {
  private _finished: bool = false;

  finishRequest(): void {
    this._finished = true;
  }

  isRequestFinished(): bool {
    return this._finished;
  }

  onRequestHeaders(a: u32): FilterHeadersStatusValues {
    const root = this.getRootContext();
    const headers = this.getRequestHeaders();
    const service = this.getRootContext().getStringProperty(["cluster_name"]);
    
    this.logDebug("Performing check from wasm filter service=" + service);
    
    headers.push(this.createHeader("x-mule-hdp-service", root.getConfig().getService()));
  
    const result = root.httpCall(
      root.getConfig().getCluster(),
      headers,
      new ArrayBuffer(0), [],
      1000,
      this,
      checkCallCb
    );

    if (result != WasmResultValues.Ok) {
      this.logDebug("Auth failed http call: " + result.toString());
      this.getRootContext().sendHttpResponse(500, "Internal server error", String.UTF8.encode("Internal Server Error"), []);
    }
  
    return FilterHeadersStatusValues.StopIteration;
  }

  onRequestBody(body_buffer_length: usize, end_of_stream: bool): FilterDataStatusValues {
    // Only pass upstream if allowed
    if (this.isRequestFinished()) {
      return FilterDataStatusValues.StopIterationAndWatermark;
    }

    return FilterDataStatusValues.Continue;
  }
  onRequestTrailers(a: u32): FilterTrailersStatusValues {
    // Only pass upstream if allowed
    if (this.isRequestFinished()) {
      return FilterTrailersStatusValues.StopIteration;
    }

    return FilterTrailersStatusValues.Continue;

  }
}
