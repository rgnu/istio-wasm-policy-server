import { Context, ContextHelper, FilterHeadersStatusValues, Headers } from "@solo-io/proxy-runtime";
import { BaseContext, BaseRoot } from "./base";

export class DenierRoot extends BaseRoot {
  config: string[];

  createContext(context_id: u32): Context {
    this.logDebug("createContext() Called");

    return ContextHelper.wrap(new Denier(context_id, this));
  }

  onConfigure(size: usize): bool {
    super.onConfigure(size);
    this.config = this.getConfiguration().split("|");

    this.logDebug("onConfigure() Called Config=" + this.config.toString());

    return true;
  }
}

class Denier extends BaseContext<DenierRoot> {
  
  onRequestHeaders(a: u32): FilterHeadersStatusValues {
    const headers: Headers = [];
    const clientId         = this.getRequestHeader("x-client-id");
    const config           = this.getRootContext().config
    
    this.logDebug("onRequestHeaders() Called with ClientID=" + clientId + " Config=" + config.toString());

    if (config.includes(clientId)) {
      this.logWarn("onRequestHeaders() Unauthorized ClientID=" + clientId);   
      this.getRootContext().sendHttpResponse(401, "Unauthorized", String.UTF8.encode("Unauthorized"), headers)
      return FilterHeadersStatusValues.StopIteration;      
    }
    
    this.logInfo("onRequestHeaders() Allowed ClientID=" + clientId);
    return FilterHeadersStatusValues.Continue;
  }
}
